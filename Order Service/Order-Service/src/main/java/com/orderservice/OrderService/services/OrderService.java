package com.orderservice.OrderService.services;

import com.orderservice.OrderService.dtos.OrderDTO;
import com.orderservice.OrderService.dtos.OrderDTO;
import com.orderservice.OrderService.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository repository;

    public List<OrderDTO> getOrdersByUserId(Long id){
        List<OrderDTO> orders = null;
        try {
            orders = repository.findOrdersByUserId(id.toString())
                    .stream()
                    .map(order -> new OrderDTO(
                            order.getId().toString(),
                            order.getOrderId(),
                            order.getUserId()
                    )).collect(Collectors.toList());
        }
        catch (Exception e){
            LOGGER.warn("----Exception in UserService -> getAllUsers()"+e);
        }
        return orders;
    }

    public List<OrderDTO> getAllOrders(){
        List<OrderDTO> orders =repository.findAll().stream().map(
                orderEntity ->new OrderDTO(
                        orderEntity.getId().toString(),
                        orderEntity.getOrderId(),
                        orderEntity.getUserId().toString()
                )
        ).collect(Collectors.toList());
        return orders;
    }

}
