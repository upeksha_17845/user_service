package com.orderservice.OrderService.dtos;

public class OrderDTO {

    private String id;
    private String OrderId;
    private String userId;

    public OrderDTO() {
    }

    public OrderDTO(String id, String orderId, String userId) {
        this.id = id;
        this.OrderId = orderId;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
