package com.userservice.UserService.services;

import com.userservice.UserService.dtos.OrderDTO;
import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.entities.UserEntity;
import com.userservice.UserService.repositories.UserRepository;
import org.apache.el.util.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import sun.plugin2.main.server.AppletID;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.base-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private RestTemplateBuilder restTemplate;

    @Autowired
    private UserRepository repository;

    public List<OrderDTO> getOrdersByUserId(Long id){
        List <OrderDTO> orders = null;
        try {
            orders = restTemplate.build().getForObject(
                    orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/"+id),
                    List.class);
        } catch (Exception e){
            e.printStackTrace();
        }
        return orders;
    }

    public List<UserDTO> getAllUsers(){
        LOGGER.info("--------ENTER INTO getUserServices in UserService-------");
        List<UserDTO> users = null;
        try{
            users = repository.findAll()
                    .stream()
                    .map(userEntity -> new UserDTO(
                            userEntity.getId().toString(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )).collect(Collectors.toList());
        }
        catch
        (Exception e){
            LOGGER.warn("----Exception in UserService -> getAllUsers()"+e);
        }
        return users;
    }

    public boolean saveUser(UserDTO user) {
        try {
            if (Validations.validateUser(user)) {
                UserEntity userEntity = new UserEntity(user.getName(), user.getAge());
                repository.save(userEntity);
                return true;
            } else {
                LOGGER.info("----Validation Error---");
                return false;
            }
        } catch (Exception e) {
            LOGGER.info("-----Error gets when save user-----" + e.getMessage());
            return false;
        }
    }
}


