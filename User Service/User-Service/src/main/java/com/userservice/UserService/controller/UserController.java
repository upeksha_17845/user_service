package com.userservice.UserService.controller;

import com.userservice.UserService.dtos.OrderDTO;
import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    //localhost:8080/api/user/getAll

    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return Arrays.asList(
                new UserDTO ("1", "Thushan", "24"),
                new UserDTO ("2", "Kishani", "22"));
    }

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id){
        return userService.getOrdersByUserId(id);
    }

    @PostMapping("/save")
    public boolean authenticateUserByPost(@RequestBody Map<String,String> data){
        UserDTO user = new UserDTO(data.get("name"),data.get("age"));
        return userService.saveUser(user);}
}
